import { Controller, Get, Param, Query } from '@nestjs/common';
import { ConcertsService } from './concerts.service';

@Controller('concerts')
export class ConcertsController {
  constructor(private readonly concertsService: ConcertsService) {}
 
  @Get()
  findOne(@Query() query): string {
    return this.concertsService.getConcerts(query);
  }

}
