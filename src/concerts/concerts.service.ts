import { Injectable, BadRequestException } from '@nestjs/common';

import concerts from '../../data/concerts.json';
import bands from '../../data/bands.json';
import venues from '../../data/venues.json';

@Injectable()
export class ConcertsService {
    getConcerts(query: any): string {
        let result = [];
        if (query.bandIds != null) {
            const bandIds = query.bandIds.split(',')
            concerts.filter(concert => bandIds.includes(concert.bandId.toString()))
                    .forEach(concert =>
                        result.push({
                            "band": bands.filter(band => band.id == concert.bandId).map(band => band.name)[0],
                            "location": venues.filter(venue => venue.id == concert.venueId).map(venue => venue.name)[0],
                            "date": concert.date,
                            "latitude": venues.filter(venue => venue.id == concert.venueId).map(venue => venue.latitude)[0],
                            "longitude": venues.filter(venue => venue.id == concert.venueId).map(venue => venue.longitude)[0]
                        })
                    );
        }
        else if (query.latitude != null && query.longitude != null && query.radius != null) {
            let filtered_venues_id = venues.filter(venue =>
                        getDistanceFromLatLonInKm(venue.latitude, venue.longitude, query.latitude, query.longitude) <= query.radius)
                        .map(venue => venue.id);
            concerts.filter(concert => filtered_venues_id.includes(concert.venueId))
                    .forEach(concert =>
                        result.push({
                            "band": bands.filter(band => band.id == concert.bandId).map(band => band.name)[0],
                            "location": venues.filter(venue => venue.id == concert.venueId).map(venue => venue.name)[0],
                            "date": concert.date,
                            "latitude": venues.filter(venue => venue.id == concert.venueId).map(venue => venue.latitude)[0],
                            "longitude": venues.filter(venue => venue.id == concert.venueId).map(venue => venue.longitude)[0]
                        })
                    );
        }
        else
            throw new BadRequestException('You have to specify `bandIds` *OR* `latitude`/`longitude`/`radius`');
        if (result.length > 0)
            return `${JSON.stringify(result)}`;
        else
            return 'No data found with your criterias'
    }
}

function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2-lat1); // deg2rad below
    var dLon = deg2rad(lon2-lon1);
    var a =
        Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(dLon/2) * Math.sin(dLon/2)
        ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c; // Distance in km
    return d;
}

function deg2rad(deg) {
    return deg * (Math.PI/180)
}
