<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

[travis-image]: https://api.travis-ci.org/nestjs/nest.svg?branch=master
[travis-url]: https://travis-ci.org/nestjs/nest
[linux-image]: https://img.shields.io/travis/nestjs/nest/master.svg?label=linux
[linux-url]: https://travis-ci.org/nestjs/nest

  <p align="center">A progressive <a href="http://nodejs.org" target="blank">Node.js</a> framework for building efficient and scalable server-side applications, heavily inspired by <a href="https://angular.io" target="blank">Angular</a>.</p>
    <p align="center">
<a href="https://www.npmjs.com/~nestjscore"><img src="https://img.shields.io/npm/v/@nestjs/core.svg" alt="NPM Version" /></a>
<a href="https://www.npmjs.com/~nestjscore"><img src="https://img.shields.io/npm/l/@nestjs/core.svg" alt="Package License" /></a>
<a href="https://www.npmjs.com/~nestjscore"><img src="https://img.shields.io/npm/dm/@nestjs/core.svg" alt="NPM Downloads" /></a>
<a href="https://travis-ci.org/nestjs/nest"><img src="https://api.travis-ci.org/nestjs/nest.svg?branch=master" alt="Travis" /></a>
<a href="https://travis-ci.org/nestjs/nest"><img src="https://img.shields.io/travis/nestjs/nest/master.svg?label=linux" alt="Linux" /></a>
<a href="https://coveralls.io/github/nestjs/nest?branch=master"><img src="https://coveralls.io/repos/github/nestjs/nest/badge.svg?branch=master#5" alt="Coverage" /></a>
<a href="https://gitter.im/nestjs/nestjs?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=body_badge"><img src="https://badges.gitter.im/nestjs/nestjs.svg" alt="Gitter" /></a>
<a href="https://opencollective.com/nest#backer"><img src="https://opencollective.com/nest/backers/badge.svg" alt="Backers on Open Collective" /></a>
<a href="https://opencollective.com/nest#sponsor"><img src="https://opencollective.com/nest/sponsors/badge.svg" alt="Sponsors on Open Collective" /></a>
  <a href="https://paypal.me/kamilmysliwiec"><img src="https://img.shields.io/badge/Donate-PayPal-dc3d53.svg"/></a>
  <a href="https://twitter.com/nestframework"><img src="https://img.shields.io/twitter/follow/nestframework.svg?style=social&label=Follow"></a>
</p>
  <!--[![Backers on Open Collective](https://opencollective.com/nest/backers/badge.svg)](https://opencollective.com/nest#backer)
  [![Sponsors on Open Collective](https://opencollective.com/nest/sponsors/badge.svg)](https://opencollective.com/nest#sponsor)-->

## Description

Readme document containing my answers to the devops-test.

## Installation

Clone this repository to you local environment (computer or server).

```bash
# make sure to have NestJS CLI installed
$ npm i -g @nestjs/cli
# Download packages depedencies
$ npm install
```

## Running the app

```bash
# starting local app
$ npm run start
```

## Part 1: instructions
- Open your favorite web browser, "Google chrome" for example (or Postman).  
   Make a GET request to http://localhost:3000/concerts (You will normaly have a HTTP 400 error without parameters and a HTTP 404 error on other URLs)


You will have 2 possibilities of filtering:
- Filter by bandIds:  
   add the parameter "bandIds" (for example in the navigator: http://localhost:3000/concerts?bandIds=1,2,3)


- Filter by position:  
   add the parameters "latitude", "longitude" and "radius"  
   (for example in the navigator: http://localhost:3000/concerts?latitude=43.6432048&longitude=-79.42463769999999&radius=1)


   Both request will return you a JSON list of the differents concerts matching your criterias. If no data match your criterias,  
   the request will return you a message like "No data found with your criterias".

## Part 2: Architecture
### Questions:
For the previous step, we had 1400 bands across 376 venues, and around 20,000 events. For this step, we ask that you document in your `README.md` how you would architecture your solution if you now had 2 million bands, 10,000 venues, and 200 million events.


Describe in detail how you would store and query the data, and what kind of mechanism you would leverage to consistently deliver short response times and guarantee the highest uptime possible.


Please then answer the two following questions :


- What do you identify as possible risks in the architecture that you described in the long run, and how would you mitigate those?
- What are the key elements of your architecture that would need to be monitored, and what would you use to monitor those?

### Answers:
First of all, we need to store those JSON data into a Database on a dedicated VM (or container) if possible. We can for example use postgre:


- It is open source
- It is free (optimal for Startup with small budget)
- It can handle lot of data (terabytes)


   Then we can load our JSON into tables. One table per JSON file: "bands", "concerts" & "venues".  
   The primary key of each table will be their ID. (we can create one for the "concerts" table, with a serial primary key ID for example, that will auto increment the id each time we add a line in this table).  
   In the "concerts" table, the venueId and the bandId will be foreigns key pointing to the 2 other tables primary keys.  
   Then fulfill the databases with the JSON files (or insert new data).


   Once the database created and the tables fulfilled, we will create indexes on the database. This will help to improve short response times.


   Now to optimise the uptime, there is some solutions:  
- solution 1: create a second node (second machine or container) with the same replicated database from the first node. Then use the master/slave principe, the master is in read & write mode, and the slave is in read only mode. We then need to use a Load Balancer in front of this 2 nodes that will use "round robin" (for example) to read data. This solution will improve short response times too.  
- solution 2: create a second node (second machine or container) with the same replicated database from the first node. Then use the master/slave principe, but this time use it as failover. There is some documentation here (https://clusterlabs.github.io/PAF/). This tool can recover the master instance if it fails, or failover the request to the slave (one of the slave). This solution do not optimise short response times.


To query the data, we will launch SQL request from the backend with informations provide in the parameters's api call. The SQL request will look like this:
```sql
SELECT b.name, v.name, c.date, v.latitude, v.longitude
FROM concerts c
JOIN bands b ON c.bandId = b.id
JOIN venues v ON c.venueId = v.id
WHERE b.id IN (bandIds)
```


The possible risks for this architecture are:


- the maintaning (in the long run). To prevent it, we need to update the softwares pretty often (to prevent security issues, and potentially correct some bugs). We can use the "blue-green" deployement principe for this. It will preserve the users's service. (https://blog.myagilepartner.fr/index.php/2017/02/24/blue-green-deployment/)
- security attack (man in the middle for example). To prevent it, we can use secure protocol to communicate with the database from the backend.
- total rupture of service (power cut for example). To prevent it, we can deploy the VM/containers on differents location.


Monitoring:


In this architecture, we need to monitor somethings to prevent the database to be full or to fall out of memory. Things to be monitored will be:


- databases storage: we will monitor all the filesystems of the databases nodes, to prevent them to be full (snmp can be used)
- databases memory: we will monitor the memory on all database nodes (snmp can be used, or postgre CLI)
- databases swap: we will monitor the swap on all database nodes (snmp can be used, or postgre CLI)
- backend memory & swap: we can monitor this things on the backend too.


All this monitoring datas can be structure in a monitoring tool (Centreon for example). Then we can define a warning and a critical level to raise some alerts, to adapt the machine (add some storage or memory, or an other database node). If needed we can create a load balancer in front of the backend too.

## Part 3: Infrastructure
### Questions:
A. How would you proceed to forbid/restrict access to the cloud production database (eg: RDS), while allowing access from team members ?

B. What kind of infrastructure would you deloy to run the BandService alongside other similar services ? What would you implement in order to guarantee the best uptime ?

C. How would you achieve a continuous deployment workflow, that would allow, upon a commit in the develop branch, to deploy new versions in staging and in production without any action from the engineering team. While guaranteeing the reliability of the production environment ?

### Answers:
   A.  
   First of all, you have to disable common "root" access.  
   Then best things are to provide one access by team member. This offer security and traceability. Make them sudoers to be able to administrate the machine.  
   If there is an active directory (or something else like LDAP) it can be a good practice to give access thrown this tool.  
   It offers a "one point" access for all the tools and machines. You can define group specifics access too.


   Then we can restrict port (iptables for example) to the only ones we are using. We can by this way remap basic ports (example 22 for ssh can be remap on 2020, and so on for the database ports).  
   Like this common attacks on default ports can be avoid.


   B.  
   If you have a lot of differents services like BandService, you can provide an API platform. It will work as a rooter, and root the requests to the good API point.  
   Like this APIs can be micro services, and are totaly independant from each other. And there is only one entry point for the user/tool requesting service or data.  
   Each services will have its own infrastructure, and are not disturbed by other services. This will offer the best uptime and performances for each part of the API platform.  
   We can for example use Docker to containerise all the micro services one by one.


   C.  
- We can watch the commit on develop with a CI tool (like Jenkins, or GitlabCI).
- First action with this commit is to lint it, to see if it respects the repository rules made by the project leader. This is a first simple verification.
- Then we can launch the unit tests inside the CI tool (karma for example for JS code) if the code owns those.
- If the tests are ok, then we can create a merge request for merging develop into master or in release branch (depends on the workflow used).
- Next action will be the build of the application and the deployment of it to the staging machine (or container).
- We can use some functional tests to verify that the application has no regressions, and its main functionalities working. We can use selenium for this tests.
- Once the functional tests are ok, we can verify that the performances are ok too. We can use a performance tool to shoot on the staging target, to verify if we do not have a performance's regression.
- Then we can deploy the new package to the production (using the blue-green deployment if the environment is load balanced).
- If one of the test wen't wrong during the continuous deployment, stop the deployment, and add a comment to the gitlab merge request.

Those steps will preserve the production if there is a problem, and do not required an engeenering team member if all things go well.


## License

  Nest is [MIT licensed](LICENSE).
